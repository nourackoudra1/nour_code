# This is my README
//create a search box
var searchBar = Titanium.UI.createSearchBar({
    barColor:'#385292', 
    showCancel:false
});

$(function() {
  $.getJSON("https://ajax.googleapis.com/ajax/services/search/news", function(json) {
    var data = _.map(json.results, function(item) {
      return { text:item.text, image:item.profile_image_url };
    });
    
    var template = {
      selectedBackgroundColor:"#111",
      rowHeight:50,
      layout:[
        { type:"text", name:"text", fontSize:10, fontWeight:"normal", color:"#999", top:5, left:50 },
        { type:"image", name:"image", width:40, height:40, top:5, left:5 }
      ]
    };
    
    //create a table view
    var tableView = Titanium.UI.createTableView({
      backgroundColor:"transparent",
      borderColor:"#333",
      template:template,
      search: searchBar,
      data:data
    }, function(eventObject) {});

    Titanium.UI.currentWindow.addView(tableView);
    Titanium.UI.currentWindow.showView(tableView);
  });
});
